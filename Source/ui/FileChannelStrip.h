//
//  FileChannelStrip.h
//  Assignment2
//
//  Created by Dom Brown on 18/12/2014.
//
//

#ifndef __Assignment2__FileChannelStrip__
#define __Assignment2__FileChannelStrip__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

#include "../Audio/Audio.h"
#include "FilePlayer.h"
#include "ChannelStrip.h"
#include "FileLoader.h"
/**
 UI Channel strip that plays back audio files
 
 @see FilePlayer
 @see FileLoader
 */
class FileChannelStrip :    public Component,
                            public Button::Listener,
                            public Slider::Listener,
                            public Timer
{
public:
    /**Constructor*/
    FileChannelStrip();
    /**Destructor*/
    ~FileChannelStrip();
    /**Resizing function*/
    void resized() override;
    /**Button listener function*/
    void buttonClicked(Button* button) override;
    /**Sets up the pointer to the fileplayer*/
    void setFilePlayer (FilePlayer* filePlayer_);
    /**Sets the gain of the file being played*/
    void sliderValueChanged (Slider* slider) override;
    /**Used to refresh the level meters*/
    void timerCallback() override;
    /**Sets colour*/
    void paint(Graphics& g) override;
    
    /**returns the fileLoader which is initialised in Main Component
     
     @return FileLoader* &fileLoader*/
    FileLoader* getFileLoader() {return &fileLoader;}
    

private:
    ChannelStrip channelStrip;
    TextButton playButton;
    FilePlayer* filePlayer;

    FileLoader fileLoader;
};

#endif /* defined(__Assignment2__FileChannelStrip__) */
