//
//  MicChannelStrip.cpp
//  Assignment2
//
//  Created by Dom Brown on 03/01/2015.
//
//

#include "MicChannelStrip.h"

MicChannelStrip::MicChannelStrip()
{
    //channel strip set up
    channelStrip.getLevel()->addListener(this);
    addAndMakeVisible(channelStrip);
    startTimer(20);
    
    //on off button set up
    onOffButton.setButtonText("Off");
    onOffButton.setColour(TextButton::buttonColourId, Colours::orangered);
    onOffButton.addListener(this);
    addAndMakeVisible(onOffButton);
}
MicChannelStrip::~MicChannelStrip()
{
    
}
void MicChannelStrip::paint(Graphics& g)
{
    g.fillAll(Colours::coral);
}
void MicChannelStrip::buttonClicked(Button* button)
{
    if (button == &onOffButton)
    {
        //swaps the mic on/off state
        audio->setMicOnOffState(!audio->getMicOnOffState());
        
        if (audio->getMicOnOffState() == false) {
            onOffButton.setButtonText("On");
            audio->setMicGain(0);
            onOffButton.setColour(TextButton::buttonColourId, Colours::orangered);
        }
        else if (audio->getMicOnOffState() == true) {
            onOffButton.setButtonText("Off");
            audio->setMicGain(channelStrip.getLevel()->getValue());
            onOffButton.setColour(TextButton::buttonColourId, Colours::red);
        }
    }
}
void MicChannelStrip::sliderValueChanged(Slider* slider)
{
    if (slider == channelStrip.getLevel())
    {
        audio->setMicGain(slider->getValue());
    }
}
void MicChannelStrip::setAudio(Audio* audio_)
{
    audio = audio_;
}
void MicChannelStrip::resized()
{

    onOffButton.setBounds(0, 0, getWidth(), 40);
    channelStrip.setBounds(0, 40, getWidth(), getHeight()-40);
}
void MicChannelStrip::timerCallback()
{
    channelStrip.getLevelMeter()->setPeakLevel(audio->getInValL(), audio->getInValR());
    startTimer(20);
}