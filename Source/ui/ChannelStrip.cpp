//
//  ChannelStrip.cpp
//  Assignment2
//
//  Created by Dom Brown on 17/12/2014.
//
//

#include "ChannelStrip.h"


ChannelStrip::ChannelStrip()
{
    level.setSliderStyle(juce::Slider::SliderStyle::LinearBarVertical);
    level.setColour(Slider::ColourIds::thumbColourId, Colours::white);
    level.setRange(0.0, 2.0);
    level.setSkewFactor(0.5);
    level.addListener(this);
    addAndMakeVisible(level);
    addAndMakeVisible(levelMeter);

}
ChannelStrip::~ChannelStrip()
{
    
}
void ChannelStrip::resized()
{
    level.setBounds(0, 0, getWidth()/2, getHeight());
    levelMeter.setBounds(getWidth()/2, 0, getWidth()/2, getHeight());


}
void ChannelStrip::sliderValueChanged(Slider* slider)
{
    
}