//
//  MicChannelStrip.h
//  Assignment2
//
//  Created by Dom Brown on 03/01/2015.
//
//

#ifndef __Assignment2__MicChannelStrip__
#define __Assignment2__MicChannelStrip__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "ChannelStrip.h"
#include "Audio.h"
/**
 UI Channel Strip that controls audio input
 
 @see Audio
 */

class MicChannelStrip   :   public Component,
                            public Slider::Listener,
                            public Timer,
                            public Button::Listener
{
public:
    /**Constructor*/
    MicChannelStrip();
    /**Destructor*/
    ~MicChannelStrip();
    /**Sets up the pointer to the audio class*/
    void setAudio(Audio* audio_);
    /**Varies the gain of the audio input*/
    void sliderValueChanged(Slider* slider) override;
    /**Sets the sizes of the child components*/
    void resized() override;
    /**Refreshes the level meters at a set time period*/
    void timerCallback() override;
    /**Turns the microphone on and off*/
    void buttonClicked(Button* button) override;
    /**Sets colour*/
    void paint(Graphics& g) override;
private:
    ChannelStrip channelStrip;
    Audio* audio;
    TextButton onOffButton;
    
};

#endif /* defined(__Assignment2__MicChannelStrip__) */
