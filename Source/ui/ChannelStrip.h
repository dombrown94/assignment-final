//
//  ChannelStrip.h
//  Assignment2
//
//  Created by Dom Brown on 17/12/2014.
//
//

#ifndef __Assignment2__ChannelStrip__
#define __Assignment2__ChannelStrip__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "LevelMeters.h"

/**
 UI object used to change the gain of audio inputs or outputs
 */

class ChannelStrip :    public Component,
                        public Slider::Listener
{
public:
    /**Constructor*/
    ChannelStrip();
    /**Destructor*/
    ~ChannelStrip();
    /**Changes the sizes of the child components*/
    void resized();
    /**Empty function - used by parent components*/
    void sliderValueChanged(Slider* slider);
    
    /**Function to access the channel strip's slider value
     
     @return Slider* &level*/
    Slider* getLevel() {return &level;}
    /**Function to access the channel strip's level meter
     
     @return LevelMeters* &levelMeter*/
    LevelMeters* getLevelMeter() {return &levelMeter; }
    
private:
    float gain;
    Slider level;
    LevelMeters levelMeter;
};

#endif /* defined(__Assignment2__ChannelStrip__) */
