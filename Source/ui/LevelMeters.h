//
//  LevelMeters.h
//  Assignment2
//
//  Created by Dom Brown on 22/12/2014.
//
//

#ifndef __Assignment2__LevelMeters__
#define __Assignment2__LevelMeters__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
/**
 UI object that displays audio as a level
 
 @see ChannelStrip
 */
class LevelMeters : public Component
{
public:
    /**Constructor*/
    LevelMeters();
    /**Destructor*/
    ~LevelMeters();
    /**Sets the colour*/
    void paint(Graphics &g) override;
    /** Sets the peak of the level meter, also decides if the peak needs updating*/
    void setPeakLevel(float newPeakLevelL, float newPeakLevelR);
    
    
private:
    float peakLevelL = 0.0;
    float peakLevelR = 0.0;


};

#endif /* defined(__Assignment2__LevelMeters__) */
