//
//  MasterChannelStrip.h
//  Assignment2
//
//  Created by Dom Brown on 22/12/2014.
//
//

#ifndef __Assignment2__MasterChannelStrip__
#define __Assignment2__MasterChannelStrip__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "ChannelStrip.h"
#include "Audio.h"
/**
 UI Channel strip that deals with the overall mix of the application and controlling recording
 
 @see Record
 */
class MasterChannelStrip :  public Component,
                            public Slider::Listener,
                            public Timer,
                            public Button::Listener
{
public:
    /** Constructor */
    MasterChannelStrip();
    /** Destructor */
    ~MasterChannelStrip();
    /** This function is called when the attached channel strips slider is moved */
    void sliderValueChanged(Slider* slider) override;
    /** sets the boundaries of internal components */
    void resized() override;
    /** sets where the audio pointer is pointing to */
    void setAudio(Audio* audio_);
    /** called to implement the level meters */
    void timerCallback() override;
    /** Called when a button is clicked */
    void buttonClicked(Button* button) override;
    /**Sets colour*/
    void paint(Graphics& g) override;
    
private:
    ChannelStrip channelStrip;
    Audio* audio;
    TextButton recordButton;
    TextButton saveButton;
};

#endif /* defined(__Assignment2__MasterChannelStrip__) */
