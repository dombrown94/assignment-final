//
//  Record.cpp
//  Assignment2
//
//  Created by Dom Brown on 20/01/2015.
//
//

#include "Record.h"
#include "../../JuceLibraryCode/JuceHeader.h"

Record::Record()
{

    audioTextFileJUCEL = File::getSpecialLocation(File::userApplicationDataDirectory).getChildFile("audioL.txt");
    DBG(audioTextFileJUCEL.getFullPathName());
    fosL = audioTextFileJUCEL.createOutputStream();
    audioTextFileJUCER = File::getSpecialLocation(File::userApplicationDataDirectory).getChildFile("audioR.txt");
    fosR = audioTextFileJUCER.createOutputStream();
}
Record::~Record()
{

    audioTextFileJUCEL.deleteFile();
    audioTextFileJUCER.deleteFile();
}
void Record::setRecording(bool newState)
{
    recordState = newState;
    
    if (recordState == true) {
        recordIndex = 0;
        fosL = audioTextFileJUCEL.createOutputStream();
        fosL->setPosition(0); // sets the record position to the start of the file
        fosR = audioTextFileJUCER.createOutputStream();
        fosR->setPosition(0); // sets the record position to the start of the file
    }
    if (recordState == false) {
        fosL->truncate(); // cuts off any eccess values after the final write position
        fosR->truncate();
    }
}
bool Record::isRecording()
{
    return recordState;
}
void Record::recordToFile(float sampleL, float sampleR)
{
    if (recordState == true)
    {
        fosL->writeFloat(sampleL);
        fosR->writeFloat(sampleR);

        recordIndex++;
    }
}
void Record::saveToFile()
{
    recordState = false;
    ScopedPointer<FileInputStream> fisL (audioTextFileJUCEL.createInputStream());
    ScopedPointer<FileInputStream> fisR (audioTextFileJUCER.createInputStream());
    
    float currentSampleL, currentSampleR;
    //Sets up an audio sample buffer as long as necessary
    AudioSampleBuffer audioRecordBuffer;
    audioRecordBuffer.setSize(2, recordIndex);
    audioRecordBuffer.clear();
    DBG(recordIndex);
    
    //Transfer the values from the text file into the audio buffer
    for (int index = 0; index < recordIndex; index++) {
        
        currentSampleL = fisL->readFloat();
        currentSampleR = fisR->readFloat();
        
        float* audioSampleL = audioRecordBuffer.getWritePointer(0, index);
        *audioSampleL = currentSampleL;
        float* audioSampleR = audioRecordBuffer.getWritePointer(1, index);
        *audioSampleR = currentSampleR;
    }
    //Save the audio buffer as a wav file
    FileChooser chooser ("Please select a file...", File::getSpecialLocation(File::userDesktopDirectory), "*wav");
    if (chooser.browseForFileToSave(true)) {
        File file (chooser.getResult().withFileExtension(".wav"));
        WavAudioFormat wavFormat;
        OutputStream* outStream = file.createOutputStream();
        AudioFormatWriter* writer = wavFormat.createWriterFor(outStream, 44100, 2, 16, NULL, 0);
        writer->writeFromAudioSampleBuffer(audioRecordBuffer, 0, recordIndex);
        delete writer;
    }
}