//
//  Audio.h
//  Assignment2
//
//  Created by Dom Brown on 15/12/2014.
//
//

#ifndef __Assignment2__Audio__
#define __Assignment2__Audio__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"
#include "Record.h"

#define NUM_FILE_PLAYERS 4
/**
 Class for dealing with audio input and output
 */
class Audio :   public AudioIODeviceCallback
{
public:
    /**Constructor*/
    Audio();
    /**Destructor*/
    ~Audio();
    /**Audio Process Function: all audio processing is done here*/
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    /**Called when audio device is started*/
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /**Called when audio device stopped*/
    void audioDeviceStopped();
    
    /**Sets the overall gain*/
    void setGain (float inGain);
    /**Sets the recording state*/
    void setRecording(bool newState);
    /**@return AudioSourcePlayer*/
    AudioSourcePlayer* getAudioSourcePlayer() { return &audioSourcePlayer; }
    /**@return AudioDeviceManager*/
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager; }
    /**@return FilePlayer*/
    FilePlayer* getFilePlayer(int index) { return &filePlayer[index]; }
    /**@return MixerAudioSource*/
    MixerAudioSource* getMixerAudioSource() {return &mixerAudioSource; }
    /**@return Record*/
    Record* getRecorder() {return &recorder;}
    
    /**Returns whether the audio is being recorded
     
     @return bool recordState*/
    bool isRecording() { return recorder.isRecording(); }
    /**Returns the current peak of the audio to be used in level meters
     
     @return float peak*/
    float getPeak(int index) { return peak[index]; }
    /** this function returns the gain value of the input
     
     @return float micGain*/
    float getMicGain() {return micGain;}
    /** This function sets the gain value for the input */
    void setMicGain(float inGain);
    /**These functions return the audio in values for use in the mic channel strip level meters
     
     @return float inValL, inValR*/
    float getInValL() {return fabsf(inValL);}
    float getInValR() {return fabsf(inValR);}
    /**Sets whether the Microphone is on or off*/
    void setMicOnOffState(bool newOnOffState);
    /**Returns whether the Microphone is on or off
     
     @return bool micOnOffState*/
    bool getMicOnOffState() {return micOnOffState;}
    
    
private:
    AudioDeviceManager audioDeviceManager;
    AudioSourcePlayer audioSourcePlayer;
    FilePlayer filePlayer[NUM_FILE_PLAYERS];
    MixerAudioSource mixerAudioSource;
    
    Record recorder;
    
    int sampleIndex = 0;
    int bufferSizeIndex;
    float gain = 0.0;
    float micGain = 0.0;
    float peak[2];
    
    float inValL;
    float inValR;
    
    bool recordState = false;
    bool micOnOffState = false;
};

#endif /* defined(__Assignment2__Audio__) */
