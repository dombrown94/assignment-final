//
//  Record.h
//  Assignment2
//
//  Created by Dom Brown on 20/01/2015.
//
//

#ifndef __Assignment2__Record__
#define __Assignment2__Record__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
/**
 Class for dealing with recording audio
 
 @see Audio
 @see MasterChannelStrip
 */

class Record
{
public:
    /**Constructor*/
    Record();
    /**Destructor*/
    ~Record();
    /**Starts and stops the recording process*/
    void setRecording(bool newState);
    /**Returns whether or not the recorder is recording*/
    bool isRecording();
    /**Records the audio sample buffer into a seperate text file*/
    void recordToFile(float sampleL, float sampleR);
    /**Takes the audio data from the file and turns it into a wav file*/
    void saveToFile();

private:
    bool recordState;
    long recordIndex = 0;
    File audioTextFileJUCEL, audioTextFileJUCER;
    ScopedPointer<FileOutputStream> fosL;
    ScopedPointer<FileOutputStream> fosR;
};

#endif /* defined(__Assignment2__Record__) */
