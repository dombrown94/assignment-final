/*
  ==============================================================================

    FilePlayer.h
    Created: 22 Jan 2013 2:49:14pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_FILEPLAYER
#define H_FILEPLAYER

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Simple FilePlayer class - strams audio from a file.
 
 @see FileLoader
 @see FileChannelStrip
 */
class FilePlayer : public AudioSource
{
public:
    /**Constructor*/
    FilePlayer();
    /**Destructor*/
    ~FilePlayer();
    /**Starts or stops playback of the looper*/
    void setPlaying (const bool newState);
    /**Gets the current playback state of the looper*/
    bool isPlaying () const;
    /**Loads the specified file into the transport source*/
    void loadFile(const File& newFile);
    /**Sets new play position*/
    void setPosition(float newPosition);
    /**Gets current playing position*/
    float getPosition();
    /**Gets length of file in seconds*/
    float getLengthOfFile();
    /**Sets the gain of the fileplayer*/
    void setGain(float inGain);
    /**returns the gain of the fileplayer*/
    float getGain();
    /**AudioSource*/
    /**Tells the fileplayer to prepare playback*/
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate);
    /**Releases the fileplayers audio once playback has stopped*/
    void releaseResources();
    /**Fetches new audio data*/
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill);
    
    /**@return AudioTransportSource*/
    AudioTransportSource* getAudioTransportSource() { return &audioTransportSource; }
    /**@return float*/
    float getRMS(int index) {return RMS[index]; }
    
private:
    AudioFormatReaderSource* currentAudioFileSource;    //reads audio from the file
    AudioTransportSource audioTransportSource;	// this controls the playback of a positionable audio stream, handling the
                                            // starting/stopping and sample-rate conversion
    TimeSliceThread thread;                 //thread for the transport source
    
    float gain = 0.0;
    float RMS[2] = {0.0, 0.0};
};

#endif  // H_FILEPLAYER
